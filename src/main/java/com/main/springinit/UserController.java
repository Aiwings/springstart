package com.main.springinit;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import java.util.Optional;



import java.util.List;
import com.main.springinit.entity.User;
import com.main.springinit.services.UserServices;



@RestController
public class UserController {
	
	@Autowired
	private UserServices service;
	
	@GetMapping("/users/{id}")
	public Optional<User> getUser(@PathVariable int id) {
		return service.getUser(id);
	}
	
	@GetMapping("/users/?name={name}")
	public List<User> getUser(@PathVariable String name) {
		return service.getByLastname(name);
	}
	
	@GetMapping("/users")
	public List<User> getUser() {
		return service.getAllUsers();
	}
	
	@DeleteMapping("/users/{id}" )
	public ResponseEntity<String> DeleteUser(@PathVariable int id) {
		boolean deleted = service.deleteUser(id);
		if(deleted) {
			return new ResponseEntity<>("{\"Success\" :\"true\"}", HttpStatus.OK );
		}
		return new ResponseEntity<>("{\"Success\" :\"false\"}", HttpStatus.NOT_FOUND );
	}
	@PostMapping("/users")
	public User addUser(@RequestBody User new_user) {
		return service.addUser(new_user);
	}
	
	@PutMapping("/users/{id}")
    public User updateUser(@RequestBody User new_user, @PathVariable int id) {
		return service.addorModify(id, new_user);
    }

}
