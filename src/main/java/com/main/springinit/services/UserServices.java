package com.main.springinit.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;
import java.util.List;

import com.main.springinit.entity.User;
import com.main.springinit.repository.UserRepository;

@Service
public class UserServices {

	@Autowired
	private UserRepository repository;
	
	public Optional<User> getUser(int id){
		return repository.findById(id);
	}
	
	public List<User>getAllUsers() {
		return repository.findAll();
	}
	
	public List<User>getByLastname(String lastname) {
		return repository.findByLastName(lastname);
	}
	public boolean deleteUser(int id) {
		boolean exist = repository.existsById(id);
		if(!exist) {
			return false;
		}
		repository.deleteById(id);
		return true;
	}
	
	public User addorModify(int id, User new_user) {
		return repository.findById(id).map(
				user -> modifyUser(user,new_user)
			).orElseGet(() -> addUserWithId(id,new_user));
	}
	
	private User modifyUser(User user, User new_user) {
		user.setFirstname(new_user.getFirstname());
        user.setLastname(new_user.getLastname());
        user.setAge(new_user.getAge());
        return repository.save(user);
	}
	public User addUser(User user) {
		return this.repository.save(user);
	}
	private User addUserWithId(int id , User new_user) {
		//new_user.setId(id);
		return repository.save(new_user);
	}
	
}
