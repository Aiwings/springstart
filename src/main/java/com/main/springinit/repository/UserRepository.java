package com.main.springinit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.main.springinit.entity.User;



public interface UserRepository extends JpaRepository<User, Integer> {
	
	List<User> findByLastName(String lastname);
	
}
